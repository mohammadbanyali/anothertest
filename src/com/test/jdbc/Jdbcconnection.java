/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mohammad
 */

//singilton

public class Jdbcconnection {
    private static Connection connection;
    static Connection readConnection()
    {
        try{
            if(connection == null)
            {
                connection=DriverManager.getConnection("jdbc:derby://localhost:1527/sample","app","app");
            }
        }
        catch(SQLException ex)
        {
            ex.printStackTrace();
        }
       
        return connection;
    }
    
}
