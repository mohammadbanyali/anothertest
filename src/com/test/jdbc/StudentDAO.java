/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.jdbc;

import com.test.model.Student;
import java.util.List;

/**
 *
 * @author mohammad
 */
public interface StudentDAO {
    public void addStudent(Student student );
    public List<Student> readStuednts();
    
}
