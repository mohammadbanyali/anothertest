/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.model;

/**
 *
 * @author mohammad
 */
public class Student {
    
    private int std_id;
    private String std_name;
    private String std_address;
    private String std_majer;

    public int getStd_id() {
        return std_id;
    }

    public void setStd_id(int std_id) {
        this.std_id = std_id;
    }

 
    public String getStd_name() {
        return std_name;
    }

    public void setStd_name(String std_name) {
        this.std_name = std_name;
    }

    public String getStd_address() {
        return std_address;
    }

    public void setStd_address(String std_address) {
        this.std_address = std_address;
    }

    public String getStd_majer() {
        return std_majer;
    }

    public void setStd_majer(String std_majer) {
        this.std_majer = std_majer;
    }
    
      @Override
    public String toString() {
        return "Student{" + "std_id=" + std_id + ", std_name=" + std_name + ", std_address=" + std_address + ", std_majer=" + std_majer + '}';
    }
    
}
